package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Assertions;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.easymock.EasyMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matcher.*;

public class ResidentTest {

    ResidentRepository stub = new ResidentRepositoryStub();

    @Test
    public void getFilteredResidentsList() {
        BaseResidentService baseResidentService = new BaseResidentService();
        baseResidentService.setResidentRepository(stub);

        List<Resident> filtered = baseResidentService.getFilteredResidentsList(new Resident("", "", "", "", new Date(2001,10,10)));
        assertEquals(1, filtered.size());
        assertEquals("Spongebob", filtered.get(0).getGivenName());

        Resident filteredResident = new Resident("S*", "", "", "", new Date(2001,10,10));
        filtered = baseResidentService.getFilteredResidentsList(filteredResident);
        assertEquals(1, filtered.size());
        assertEquals("Mart", filtered.get(0).getFamilyName());

        filtered = baseResidentService.getFilteredResidentsList(new Resident("s*", "", "", "", new Date(2001,10,10)));
        assertEquals(1, filtered.size());
        assertEquals("Spongebob", filtered.get(0).getGivenName());

        filtered = baseResidentService.getFilteredResidentsList(new Resident("", "", "", "", new Date(1905, 5, 9)));
        assertEquals(1, filtered.size());
        assertEquals("Patrick", filtered.get(0).getGivenName());

        filtered = baseResidentService.getFilteredResidentsList(new Resident("", "K*", "", "",null));
        assertEquals(1, filtered.size());
        assertEquals("Patrick", filtered.get(0).getGivenName());   //0 für den given name
    }

    @Test
    public void getUniqueResidentTest() {
        ResidentRepository stub = new ResidentRepositoryStub();
        BaseResidentService baseResidentService = new BaseResidentService();
        baseResidentService.setResidentRepository(stub);

        try {
            Resident unique = baseResidentService.getUniqueResident(new Resident("", "", "", "", new Date(2001,10,10)));
            Assertions.assertEquals("Spongebob", unique.getGivenName());
        } catch(ResidentServiceException e) {
            fail("Exception Date");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception, Date");
        }

        try {
            Resident unique = baseResidentService.getUniqueResident(new Resident("spongebob", "", "", "", new Date(2001,10,10)));
            Assertions.assertEquals("Spongebob", unique.getGivenName());
            Assertions.assertEquals("Mart", unique.getFamilyName());
        } catch(ResidentServiceException e) {
            fail("Exception Date, Name");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception, Date Name");
        }

         try {
             Resident unique = baseResidentService.getUniqueResident(new Resident("spongebob", "Mart", "Stutt", "Straße21", new Date(2001,10,10)));
             fail("Found Unique Resident, that doesn't exist");
         } catch(ResidentServiceException e) {
             Assertions.assertEquals(e.getMessage(), "Suchanfrage lieferte kein eindeutiges Ergebnis!");
         } catch (Exception e) {
             fail("Wrongfully thrown Exception for All");
         }



        try {
            // Ruft die getUniqueResident-Methode auf, sollte eine Exception werfen
            baseResidentService.getUniqueResident(new Resident("Sp*ngebob", "Mart", "Straße1", "Stutt", new Date(2001, 10, 10)));

            // Falls keine Exception auftritt, ist der Test fehlgeschlagen
            fail("Expected ResidentServiceException for Wildcards, but no exception was thrown");
        } catch (ResidentServiceException e) {
            // Überprüft, ob die erwartete Exception mit der richtigen Nachricht geworfen wurde
            assertEquals("Wildcards (*) sind nicht erlaubt!", e.getMessage());
        } catch (Exception e) {
            // Falls eine andere Exception auftritt, ist der Test ebenfalls fehlgeschlagen
            fail("Unexpected Exception: " + e.getMessage());
        }


    }

    @Test
    public void MockTest(){
        ResidentRepository residentRepositoryMock = createMock(ResidentRepository.class);

        Resident spongebob = new Resident("Spongebob", "Mart", "Straße1", "Stutt", new Date(2001, 10, 10));
        Resident patrick = new Resident("Patrick", "Kug", "Straße2", "Berlin", new Date(1905, 5, 9));
        Resident andy = new Resident("Andy", "Grey", "Straße3", "hano", new Date(1960, 7, 6));
        Resident gerry = new Resident("Gerry", "h2", "Straße5", "onepiece", new Date(1920, 3, 4));

        expect(residentRepositoryMock.getResidents()).andReturn(Arrays.asList(spongebob, patrick, andy, gerry));


        replay(residentRepositoryMock);

        BaseResidentService baseResidentService = new BaseResidentService();


        baseResidentService.setResidentRepository(residentRepositoryMock);

        List<Resident> filtered = baseResidentService.getFilteredResidentsList(new Resident("S*", "", "", "", new Date(2001,10,10)));


        //filtered = baseResidentService.getFilteredResidentsList(new Resident("", "", "S*", "", null));
       //assertEquals(1, filtered.size());
       //assertEquals("Spongebob", filtered.get(0).getGivenName());

        verify(residentRepositoryMock);
        assertEquals(1, filtered.size());
        assertEquals("Spongebob", filtered.get(0).getGivenName());
        assertThat(filtered.size(), equalTo(1));
        assertTrue(filtered.contains(spongebob));

    }
}
