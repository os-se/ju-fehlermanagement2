package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {


    public List<Resident> getResidents() {
        Resident spongebob = new Resident("Spongebob", "Mart", "Straße1", "Stutt", new Date(2001, 10, 10));

        Resident patrick = new Resident("Patrick", "Kug", "Straße2", "Berlin", new Date(1905, 5, 9));

        Resident andy = new Resident("Andy", "Grey", "Straße3", "hano", new Date(1960, 7, 6));

        Resident gerry = new Resident("Gerry", "h2", "Straße5", "onepiece", new Date(1920, 3, 4));

        Resident john = new Resident("John", "Cena", "StraßeX", "Berlin", new Date(2005, 1, 1));

        List<Resident> residents = Arrays.asList(spongebob, patrick, andy, gerry, john);
        return residents;
    }
}
